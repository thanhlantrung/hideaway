class CreateSwaps < ActiveRecord::Migration[5.0]
  def change
    create_table :swaps do |t|
      t.references :user, foreign_key: true
      t.references :home, foreign_key: true
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps
    end
  end
end
