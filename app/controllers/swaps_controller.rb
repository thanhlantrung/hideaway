class SwapsController < ApplicationController
	before_action :authenticate_user! 
	
	def preload
		home = Home.find(params[:home_id])
		today = Date.today
		swaps = home.swaps.where("start_date >= ? OR end_date >= ?", today, today)

		render json: swaps 
	end

	def preview
		start_date = Date.parse(params[:start_date])
		start_date = Date.parse(params[:end_date])

		output = {
			conflict: is_conflict(start_date, end_date)
		}

		render json: output 
	end

	def create
		@swap = current_user.swaps.create(swap_params)

		redirect_to @swap.home, notice: "Your swap has been requested"
	end

	#def your_trips
	#	@trips = current_user.swaps 

	#end

	#def your_swaps 
	#	@homes = current_user.homes
	#end

	private

		def is_conflict(start_date, end_date)
			home = Home.find(params[:home_id])

			check = home.reservations.where("? < start_date AND end_date < ?", start_date, end_date)
			check.size > 0? true : false 
		end
		
		def swaps_params
			params.require(:swap).permit(:start_date, :end_date, :home_id)
		end
end 