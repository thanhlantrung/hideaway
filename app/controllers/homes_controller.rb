class HomesController < ApplicationController
  #before_action :set_home, only: [:show, :edit, :update]
  #before_action :authenticate_user!, except: [:show]

  def index
   @homes = current_user.homes
  end

  def show
     @photos = @home.photos
  end

  def new
    @home = current_user.homes.build 
    
  end

  def create
    @home = current_user.homes.build(home_params)


    if @home.save

      if params[:images]
        params[:images].each do |image|
          @home.photos.create(image: image)
        end
      end

      @photos = @home.photos
      redirect_to edit_home_path(@home), notice: "Saved.."
    else
      render :new
    end
  end

  def edit
    if current_user.id == @home.user.id
      @photos = @home.photos
    else
      redirect_to root_path, notice: "You don't have permission"
    end
  end

  def update
    if @home.update(home_params)

      if params[:images]
        params[:images].each do |image|
          @home.photos.create(image: image)
        end
      end


      redirect_to edit_home_path(@home), notice: "Updated.."

      
    else
      render :ender
     end
  end

  private
    def set_home
      @home = Home.find(params[:id])
    end

    def home_params
      params.require(:home).permit(:home_type, :accommodate, :bed_room, :bath_room, :listing_name, :summary, :address, :is_air, :is_heating, :active)
    end
end
