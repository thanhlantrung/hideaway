Rails.application.routes.draw do
  


  root 'pages#home'

  devise_for  :users, 
              :path => '', 
              :path_names => {:sign_in => 'login', :sign_out => 'logout', :edit => 'profile'},
              :controllers => {:omniauth_callbacks => 'omniauth_callbacks'}
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :users, only: [:show]
  resources :homes
  resources :photos

  resources :homes do 
  	resources :swaps, only: [:create]
  end

  get '/preload' => 'swaps#preload'
  get '/preview' => 'swaps#preview'

  get '/your_trips' => 'swaps#your_trips'
  get '/your_swaps' => 'swaps#your_swaps'
end
